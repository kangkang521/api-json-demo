package com.example.api.json.demo;

import apijson.Log;
import apijson.framework.APIJSONApplication;
import apijson.framework.APIJSONCreator;
import apijson.framework.APIJSONParser;
import apijson.orm.Parser;
import apijson.orm.SQLConfig;
import apijson.orm.SQLExecutor;
import com.example.api.json.demo.config.apijson.DemoSQLConfig;
import com.example.api.json.demo.config.apijson.DemoSQLExecutor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 启动类
 *
 * @author huangxiaodong
 * @date 2021/12/17  10:28
 */
@Slf4j
@SpringBootApplication
public class Application {
    public static void main(String[] args) throws Exception {
        SpringApplication.run(Application.class, args);
        APIJSONApplication.init();
        log.info("service demo is ready for service.");
    }

    static {
        // 关闭debug打印消息
        Log.DEBUG = false;
        APIJSONApplication.DEFAULT_APIJSON_CREATOR = new APIJSONCreator() {
            @Override
            public SQLConfig createSQLConfig() {
                return new DemoSQLConfig();
            }

            @Override
            public Parser<Long> createParser() {
                return new APIJSONParser()
                        // 取消原本的校验，如果需要校验修改对应表Request
                        .setNeedVerifyLogin(false)
                        // 取消对请求内容的判断
                        .setNeedVerifyContent(false);
            }

            @Override
            public SQLExecutor createSQLExecutor() {
                return new DemoSQLExecutor();
            }
        };
    }
}
