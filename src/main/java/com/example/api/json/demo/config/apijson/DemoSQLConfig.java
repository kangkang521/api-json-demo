package com.example.api.json.demo.config.apijson;

import apijson.RequestMethod;
import apijson.column.ColumnUtil;
import apijson.framework.APIJSONSQLConfig;
import apijson.orm.AbstractSQLConfig;
import lombok.extern.slf4j.Slf4j;

import java.util.Arrays;
import java.util.List;

@Slf4j
public class DemoSQLConfig extends APIJSONSQLConfig {

    public DemoSQLConfig() {
        super();
    }

    public DemoSQLConfig(RequestMethod method, String table) {
        super(method, table);
    }

    static {
        // MYSQL, POSTGRESQL, SQLSERVER, ORACLE, DB2
        DEFAULT_DATABASE = "MYSQL";
        // 数据库的 Schema 名
        DEFAULT_SCHEMA = "test";

        SIMPLE_CALLBACK = new SimpleCallback() {
            @Override
            public AbstractSQLConfig getSQLConfig(RequestMethod method, String database, String schema, String table) {
                return new DemoSQLConfig(method, table);
            }

            // 数据库自增 id
            @Override
            public Object newId(RequestMethod method, String database, String schema, String table) {
                return null;
            }

            // apiJson默认注解为id，其他id需要额外指定
            @Override
            public String getIdKey(String database, String schema, String datasource, String table) {
                // 如果表的主键不是"id"，可以另外指定
                if ("Student".equals(table)) {
                    return "s_id";
                }
                return super.getIdKey(database, schema, datasource, table);
            }
        };
    }

    // 以下是对 Oracle的特殊处理，记得开启
//    @Override
//    public String getKey(String key) {
//        // 排除apiJson的配置表（字段都是小写），oracle默认大写
//        // 如果是配置表返回带双引号的字段，例如"id","version"
//        List<String> sysTables = Arrays.asList("Access", "Request", "Function");
//        if (sysTables.contains(getTable())) {
//            return super.getKey(key);
//        }
//        return key;
//    }
//
//    /**
//     * 如果数据源确定的话，可以指定，以减少过多数据源的判断 来提高性能
//     */
//    @Override
//    public boolean isOracle() {
//        return true;
//    }
}
